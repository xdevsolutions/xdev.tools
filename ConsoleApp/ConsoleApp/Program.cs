﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Newtonsoft.Json;
using Syndicates;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Tentativa de Ler arquivo PDF
            //PdfHelper.ReadPdfFile(); - //Funcionando

            // Ler arquivo Excel
            SpreadsheetHelper.ReadExcelFile();
        }        
    }

    public class PdfHelper
    {
        public static string ReadPdfFile()
        {
            var filePath = @"D:\Temp\2018\Syndicates\Quadro_Funcional_122017.pdf";

            PdfReader reader = new PdfReader(filePath);
            var text = string.Empty;
            var lines = new List<string>();
            var places = new List<string>();
            for (int page = 1; page <= reader.NumberOfPages; page++)
            {
                text += PdfTextExtractor.GetTextFromPage(reader, page);

                var columns = text.Split(new char[] { '\n' }).ToList();
                var withoutHeaders = columns.Where(t =>
                t != "Prefeitura do Município de Araucária" &&
                t != "Relação de Servidores" &&
                t != "Dezembro de 2017" &&
                t != "Horas" &&
                t != "Nome Local de Lotação" &&
                t != "Cargo Investidura Semanal Horário").ToList();

                if (withoutHeaders.Any())
                {
                    lines.AddRange(withoutHeaders);
                }
            }
            reader.Close();
            reader.Dispose();

            if (lines.Any())
            {
                places = lines.Where(l => 
                    !l.Contains("Concurso") &&
                    !l.Contains("Comissão") &&
                    !l.Contains("Anterior") &&
                    !l.Contains("Tutelar") &&
                    !l.Contains("Simplificado") &&
                    !l.Contains("Município"))
                    .Select(l =>
                    {
                        return l;
                    })
                    .Distinct()                    
                    .ToList();

                if (places.Any())
                {
                    var placeNames = new List<string>();

                    var current = string.Empty;
                    var previous = string.Empty;
                    var next = string.Empty;
                    for (int i = 0; i < places.Count; i++)
                    {
                        current = places[i];

                        if (i == 0)
                            previous = current;
                        else
                            previous = places[i - 1];

                        if (i + 1 < places.Count)
                            next = places[i + 1];

                        // Validation
                        if (next.StartsWith("-"))
                        {
                            current = current + next; 
                        }
                        else if (previous.EndsWith("- "))
                        {
                            current = previous + current;
                        }

                        if (current.StartsWith("-") || current.EndsWith("- "))
                            continue;

                        placeNames.Add(current);
                    }

                    if (!placeNames.Any())
                        return null;

                    using (var outFile = new StreamWriter(filePath.Replace(".pdf", "-places.txt"), false, Encoding.UTF8))
                    {
                        placeNames
                            //.OrderBy(l => l).ToList()
                            .ForEach(p => outFile.WriteLine(p));
                    }
                }
            }

            return text;
        }

        public static void ParsePdfFile()
        {
            var filePath = @"D:\Temp\2018\Syndicates\Quadro_Funcional_122017.pdf";

            var parser = new PdfParser();
            parser.ExtractText(filePath, filePath.Replace(".pdf", ".txt"));
        }

        /// <summary>
        /// Parses a PDF file and extracts the text from it.
        /// </summary>
        public class PdfParser
        {
            /// BT = Beginning of a text object operator 
            /// ET = End of a text object operator
            /// Td move to the start of next line
            ///  5 Ts = superscript
            /// -5 Ts = subscript

            #region Fields

            #region _numberOfCharsToKeep

            /// <summary>
            /// The number of characters to keep, when extracting text.
            /// </summary>
            private static int _numberOfCharsToKeep = 15;

            #endregion

            #endregion

            #region ExtractText

            /// <summary>
            /// Extracts a text from a PDF file.
            /// </summary>
            /// <param name="inFileName">the full path to the pdf file.</param>
            /// <param name="outFileName">the output file name.</param>
            /// <returns>the extracted text</returns>
            public bool ExtractText(string inFileName, string outFileName)
            {
                StreamWriter outFile = null;
                try
                {
                    // Create a reader for the given PDF file
                    PdfReader reader = new PdfReader(inFileName);
                    //outFile = File.CreateText(outFileName);
                    outFile = new StreamWriter(outFileName, false, System.Text.Encoding.UTF8);

                    Console.Write("Processing: ");

                    int totalLen = 68;
                    float charUnit = ((float)totalLen) / (float)reader.NumberOfPages;
                    int totalWritten = 0;
                    float curUnit = 0;

                    for (int page = 1; page <= reader.NumberOfPages; page++)
                    {
                        outFile.Write(ExtractTextFromPDFBytes(reader.GetPageContent(page)) + " ");

                        // Write the progress.
                        if (charUnit >= 1.0f)
                        {
                            for (int i = 0; i < (int)charUnit; i++)
                            {
                                Console.Write("#");
                                totalWritten++;
                            }
                        }
                        else
                        {
                            curUnit += charUnit;
                            if (curUnit >= 1.0f)
                            {
                                for (int i = 0; i < (int)curUnit; i++)
                                {
                                    Console.Write("#");
                                    totalWritten++;
                                }
                                curUnit = 0;
                            }

                        }
                    }

                    if (totalWritten < totalLen)
                    {
                        for (int i = 0; i < (totalLen - totalWritten); i++)
                        {
                            Console.Write("#");
                        }
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
                finally
                {
                    if (outFile != null) outFile.Close();
                }
            }

            #endregion

            #region ExtractTextFromPDFBytes

            /// <summary>
            /// This method processes an uncompressed Adobe (text) object 
            /// and extracts text.
            /// </summary>
            /// <param name="input">uncompressed</param>
            /// <returns></returns>
            public string ExtractTextFromPDFBytes(byte[] input)
            {
                if (input == null || input.Length == 0) return "";

                try
                {
                    string resultString = "";

                    // Flag showing if we are we currently inside a text object
                    bool inTextObject = false;

                    // Flag showing if the next character is literal 
                    // e.g. '\\' to get a '\' character or '\(' to get '('
                    bool nextLiteral = false;

                    // () Bracket nesting level. Text appears inside ()
                    int bracketDepth = 0;

                    // Keep previous chars to get extract numbers etc.:
                    char[] previousCharacters = new char[_numberOfCharsToKeep];
                    for (int j = 0; j < _numberOfCharsToKeep; j++) previousCharacters[j] = ' ';


                    for (int i = 0; i < input.Length; i++)
                    {
                        char c = (char)input[i];
                        if (input[i] == 213)
                            c = "'".ToCharArray()[0];

                        if (inTextObject)
                        {
                            // Position the text
                            if (bracketDepth == 0)
                            {
                                if (CheckToken(new string[] { "TD", "Td" }, previousCharacters))
                                {
                                    resultString += "\n\r";
                                }
                                else
                                {
                                    if (CheckToken(new string[] { "'", "T*", "\"" }, previousCharacters))
                                    {
                                        resultString += "\n";
                                    }
                                    else
                                    {
                                        if (CheckToken(new string[] { "Tj" }, previousCharacters))
                                        {
                                            resultString += " ";
                                        }
                                    }
                                }
                            }

                            // End of a text object, also go to a new line.
                            if (bracketDepth == 0 &&
                                CheckToken(new string[] { "ET" }, previousCharacters))
                            {

                                inTextObject = false;
                                resultString += " ";
                            }
                            else
                            {
                                // Start outputting text
                                if ((c == '(') && (bracketDepth == 0) && (!nextLiteral))
                                {
                                    bracketDepth = 1;
                                }
                                else
                                {
                                    // Stop outputting text
                                    if ((c == ')') && (bracketDepth == 1) && (!nextLiteral))
                                    {
                                        bracketDepth = 0;
                                    }
                                    else
                                    {
                                        // Just a normal text character:
                                        if (bracketDepth == 1)
                                        {
                                            // Only print out next character no matter what. 
                                            // Do not interpret.
                                            if (c == '\\' && !nextLiteral)
                                            {
                                                resultString += c.ToString();
                                                nextLiteral = true;
                                            }
                                            else
                                            {
                                                if (((c >= ' ') && (c <= '~')) ||
                                                    ((c >= 128) && (c < 255)))
                                                {
                                                    resultString += c.ToString();
                                                }

                                                nextLiteral = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // Store the recent characters for 
                        // when we have to go back for a checking
                        for (int j = 0; j < _numberOfCharsToKeep - 1; j++)
                        {
                            previousCharacters[j] = previousCharacters[j + 1];
                        }
                        previousCharacters[_numberOfCharsToKeep - 1] = c;

                        // Start of a text object
                        if (!inTextObject && CheckToken(new string[] { "BT" }, previousCharacters))
                        {
                            inTextObject = true;
                        }
                    }

                    return CleanupContent(resultString);
                }
                catch
                {
                    return "";
                }
            }

            private string CleanupContent(string text)
            {
                string[] patterns = { @"\\\(", @"\\\)", @"\\226", @"\\222", @"\\223", @"\\224", @"\\340", @"\\342", @"\\344", @"\\300", @"\\302", @"\\304", @"\\351", @"\\350", @"\\352", @"\\353", @"\\311", @"\\310", @"\\312", @"\\313", @"\\362", @"\\364", @"\\366", @"\\322", @"\\324", @"\\326", @"\\354", @"\\356", @"\\357", @"\\314", @"\\316", @"\\317", @"\\347", @"\\307", @"\\371", @"\\373", @"\\374", @"\\331", @"\\333", @"\\334", @"\\256", @"\\231", @"\\253", @"\\273", @"\\251", @"\\221" };
                string[] replace = { "(", ")", "-", "'", "\"", "\"", "à", "â", "ä", "À", "Â", "Ä", "é", "è", "ê", "ë", "É", "È", "Ê", "Ë", "ò", "ô", "ö", "Ò", "Ô", "Ö", "ì", "î", "ï", "Ì", "Î", "Ï", "ç", "Ç", "ù", "û", "ü", "Ù", "Û", "Ü", "®", "™", "«", "»", "©", "'" };

                for (int i = 0; i < patterns.Length; i++)
                {
                    string regExPattern = patterns[i];
                    Regex regex = new Regex(regExPattern, RegexOptions.IgnoreCase);
                    text = regex.Replace(text, replace[i]);
                }

                return text;
            }

            #endregion

            #region CheckToken

            /// <summary>
            /// Check if a certain 2 character token just came along (e.g. BT)
            /// </summary>
            /// <param name="tokens">the searched token</param>
            /// <param name="recent">the recent character array</param>
            /// <returns></returns>
            private bool CheckToken(string[] tokens, char[] recent)
            {
                foreach (string token in tokens)
                {
                    if ((recent[_numberOfCharsToKeep - 3] == token[0]) &&
                        (recent[_numberOfCharsToKeep - 2] == token[1]) &&
                        ((recent[_numberOfCharsToKeep - 1] == ' ') ||
                        (recent[_numberOfCharsToKeep - 1] == 0x0d) ||
                        (recent[_numberOfCharsToKeep - 1] == 0x0a)) &&
                        ((recent[_numberOfCharsToKeep - 4] == ' ') ||
                        (recent[_numberOfCharsToKeep - 4] == 0x0d) ||
                        (recent[_numberOfCharsToKeep - 4] == 0x0a))
                        )
                    {
                        return true;
                    }
                }
                return false;
            }

            #endregion
        }
    }

    public class SpreadsheetHelper
    {
        public static void ReadExcelFile()
        {
            var filePath = @"D:\Temp\2018\Syndicates\Planilha MALOTE_001.xls";
            var resultFilePath = @"D:\Temp\2018\Syndicates\LocaisDeTrabalho_001.json";

            var connectionString = $@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={filePath};Extended Properties='Excel 8.0;HDR=Yes;'";
            //connectionString = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={filePath}; Extended Properties=Excel 12.0;";
            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                var locais = new List<LocalDeTrabalho>();
                try
                {
                    connection.Open();

                    DataTable dtSchema = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if ((null == dtSchema) || (dtSchema.Rows.Count <= 0))
                    {
                        //raise exception if needed
                        return;
                    }
                    
                    for (int i = 0; i < dtSchema.Rows.Count; i++)
                    {
                        //Reading the first sheet name from the Excel file.
                        var sheetName = dtSchema.Rows[i]["TABLE_NAME"].ToString();
                        var dataSet = new DataSet();

                        new OleDbDataAdapter("SELECT * FROM [" + sheetName + "]", connection).Fill(dataSet);
                        if(dataSet.Tables != null && dataSet.Tables.Count > 0)
                        {
                            for (int x = 0; x < dataSet.Tables.Count; x++)
                            {
                                DataTable dataTable = dataSet.Tables[x];
                                if (dataTable.Rows != null && dataTable.Rows.Count > 0)
                                {
                                    GetLocaisDeTrabalho(locais, dataTable);
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    // Clean up.
                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }

                using (var outFile = new StreamWriter(resultFilePath, false, Encoding.UTF8))
                {
                    var locaisDeTrabalho = JsonConvert.SerializeObject(locais.ToArray());
                    outFile.WriteLine(locaisDeTrabalho);
                }
            }
        }

        private static void GetLocaisDeTrabalho(List<LocalDeTrabalho> locais, DataTable dataTable)
        {
            for (int j = 0; j < dataTable.Rows.Count; j++)
            {
                var local = new LocalDeTrabalho
                {
                    Ativo = true                    
                };
                var itemArray = dataTable.Rows[j].ItemArray;
                if (itemArray != null)
                {
                    // itemArray[0] Local
                    if (IsNotNull(itemArray[0]))
                    {
                        local.Nome = itemArray[0].ToString().ToUpper();
                    }

                    // itemArray[1] Contato
                    if (IsNotNull(itemArray[1]))
                    {
                        local.Representante = GetRepresentante(itemArray[1]);

                        local.Telefones = GetTelefones(local.Representante);
                    }

                    // itemArray[2] Área
                    if (IsNotNull(itemArray[2]))
                    {
                        local.Area = itemArray[2].ToString().ToUpper();
                    }

                    // itemArray[3] Endereço
                    if (IsNotNull(itemArray[3]))
                    {
                        local.Endereco = GetEndereco(itemArray);
                    }

                    if(IsNotNull(itemArray[0]))
                        locais.Add(local);
                }
            }
        }

        private static bool IsNotNull(object itemArray)
        {
            return itemArray != null && !(itemArray is DBNull);
        }

        private static IEnumerable<string> GetTelefones(string representante)
        {
            var result = string.Empty;
            if (representante.Contains("-"))
            {
                //99672-7877 or Débora 9945-8727
                var splits = representante.Split('-');
                var first = string.Join("", representante.Split('-')[0].ToCharArray().Where(c => char.IsNumber(c)).ToArray());
                var second = string.Join("", representante.Split('-')[1].ToCharArray().Where(c => char.IsNumber(c)).ToArray());

                result = JsonConvert.SerializeObject(new { TelefoneCelular = $"{first}-{second}" });

                return new List<string> { result };
            }

            return null;
        }

        private static Endereco GetEndereco(object[] columns)
        {
            var result = new Endereco
            {
                Cidade = "Araucária",
                Uf = "PR",
                DataAtualizacao = DateTime.Now
            };

            if (columns[3] is string)
            {
                var endereco = columns[3].ToString();
                if (endereco.Contains(","))
                {
                    if (endereco.Split(',') != null)
                    {
                        result.Logradouro = endereco.Split(',').FirstOrDefault().ToUpper();
                        result.Numero = endereco.Split(',')[1].ToUpper();
                    }
                    else
                    {
                        result.Logradouro = endereco.ToUpper();
                    }
                }

                if (endereco.Contains("(") && endereco.Contains(")"))
                {
                    result.Complemento = endereco.Split('(', ')').Length > 1 ? endereco.Split('(', ')')[1].ToUpper() : string.Empty;
                }
            }
            // Bairro
            if (columns[4] is string)
            {
                var bairro = columns[4].ToString();
                if (bairro.Contains("Jd."))
                {
                    bairro = bairro.Replace("Jd.", "Jardim");
                }
                result.Bairro = bairro.ToUpper();
            }
            return result;
        }

        private static string GetRepresentante(object column)
        {
            if (column is string)
            {
                var result = string.Empty;
                var contato = column.ToString();
                if (contato.Contains("/"))
                {
                    result = string.Join(" / ", contato.Split('/').Select(l => { return l.Trim(); }).ToArray() );
                }
                else
                {
                    result = contato;
                }
                return result.ToUpper();
            }
            return string.Empty;
        }
    }
}

namespace Syndicates
{
    public class LocalDeTrabalho
    {
        public string Nome { get; set; }

        public DateTime DataCadastro { get; set; }

        public Endereco Endereco { get; set; }

        public string Area { get; set; }

        public string Representante { get; set; }

        public IEnumerable<string> Telefones { get; set; }

        public string Email { get; set; }

        public int QuantidadeFiliados { get; set; }

        public int QuantidadeNaoFiliados { get; set; }

        public int QuantidadeDesfiliados { get; set; }

        public bool Ativo { get; set; }
    }

    public class Endereco
    {
        public DateTime? DataAtualizacao { get; set; }

        public string Logradouro { get; set; }

        public string Numero { get; set; }

        public string Complemento { get; set; }

        public string Cep { get; set; }

        public string Bairro { get; set; }

        public string Cidade { get; set; }

        public string Uf { get; set; }
    }
}